<?php

declare(strict_types=1);

namespace Touch5\T5SalAnimation\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 *
 * @author Michael Scheunemann <m.scheunemann@touch5.de>
 */
class AnimationTest extends UnitTestCase
{
    /**
     * @var \T5\T5SalAnimation\Domain\Model\Animation|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function  setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \Touch5\T5SalAnimation\Domain\Model\Animation::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getAnimationFlexboxReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAnimationFlexbox()
        );
    }

    /**
     * @test
     */
    public function setAnimationFlexboxForStringSetsAnimationFlexbox(): void
    {
        $this->subject->setAnimationFlexbox('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('animationFlexbox'));
    }
}
