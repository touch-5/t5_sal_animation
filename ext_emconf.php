<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'sal Animation',
    'description' => 'Animate content elements on page scrolling with sal.js',
    'category' => 'plugin',
    'author' => 'Michael Scheunemann',
    'author_email' => 'm.scheunemann@touch5.de',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '1.1.9',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-12.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
