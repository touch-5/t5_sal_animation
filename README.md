# t5 sal animation 
`t5_sal_animation` is a extension for the TYPO3 content management system witch using Sal (Scroll Animation Library).

### What does it do
Sal (Scroll Animation Library) was created to provide a performant and lightweight solution for animating elements on scroll. It's based on the Intersection Observer, which gives amazing performance in terms of checking the element's presence in the viewport.