<?php
defined('TYPO3') or die();

call_user_func(function()
{
	$extensionKey = 't5_sal_animation';

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
		$extensionKey,
		'setup',
		"@import 'EXT:t5_sal_animation/Configuration/TypoScript/setup.typoscript'"
	);
});