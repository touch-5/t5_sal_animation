<?php
defined('TYPO3') || die();

(static function() {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_t5salanimation_domain_model_animation', 'EXT:t5_sal_animation/Resources/Private/Language/locallang_csh_tx_t5salanimation_domain_model_animation.xlf');
})();
