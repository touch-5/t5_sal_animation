<?php


defined('TYPO3_MODE') or die();

// Feld definieren
$tempColumns = [
	'animation_flexbox' => [
		'label' => 'LLL:EXT:t5_sal_animation/Resources/Private/Language/locallang_db.xlf:tt_content.animation_settings',
		'exclude' => 0,
		'config' => [
			'type' => 'flex',
			'ds' => [
        'default' => 'FILE:EXT:t5_sal_animation/Configuration/FlexForms/animation.xml'
			]
		]
	]
];

// Feld der allgemeinen Datensatzbeschreibung hinzufügen - noch keine Ausgabe im Backend!
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $tempColumns);

// Feld einer neuen Palette hinzufügen
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
	'tt_content',
	'animation_settings',
	'animation_flexbox'
);

// Neue Palette dem Tag hinzufügen, nach dem Titel - Dadurch Anzeige im Backend
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tt_content',
	'--div--;LLL:EXT:t5_sal_animation/Resources/Private/Language/locallang_db.xlf:tab.animation,
 --palette--;;animation_settings',
	'',
	''
);
